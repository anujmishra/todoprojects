document.querySelector("#input").addEventListener("keydown", (event) => {
    if(event.key === "Enter"){
      event.preventDefault()
      const input = document.querySelector("#input");
     // addItem(input.value);
     console.log(input.value);
     senddata(input.value)
     display()
    }
      
  });
      
  
  document.querySelector("#add_item").addEventListener("click", (e) => {
    e.preventDefault()
    const input = document.querySelector("#input");
    // addItem(input.value);
    senddata(input.value)
    display()
  });
  
   const display=()=>{
    fetch('http://localhost:3000/getdata')
    .then(res=>res.json())
    .then(data=>{
      for(let i=0;i<data.length;i++){
        let value=data[i]['jadu']
        addItem(value,i)
      }

    }).catch(err=>console.log(err))
   }
   display()
  const senddata=(data)=>{
    let one={
      jadu:data
    }

    fetch('http://localhost:3000/senddata',{
      method:"POST",
      mode:"cors",
      headers:{
        'Content-type':'application/json'
      },
      body:JSON.stringify(one)
    }).then(res=> location.reload()
    ).catch(res=>console.log(res))
  }
  addItem = (input,idx) => {
    const item = document.createElement("div");
    const div = document.createElement("div");
    const checkIcon = document.createElement("i");
    const trashIcon = document.createElement("i");
    const text = document.createElement("p");
  
    item.className = "item";
    text.textContent = input;
  
    checkIcon.className = "fas fa-check-square";
    checkIcon.style.color = "lightgray";
    checkIcon.addEventListener("click", () => {
      checkIcon.style.color = "limegreen";
    })
    div.appendChild(checkIcon);
  
    trashIcon.className = "fas fa-trash";
    trashIcon.style.color = "darkgray";
    trashIcon.addEventListener("click", () => {
      // item.remove();
      remove(idx)
    })
    div.appendChild(trashIcon);
  
    item.appendChild(text);
    item.appendChild(div);
  
    document.querySelector("#to_do_list").appendChild(item);
    document.querySelector("#input").value = "";
  }

 const remove=(idx)=>{
  let one={
    'idx':idx
  }
  fetch('http://localhost:3000/deldata',{
    method:"POST",
    mode:"cors",
    headers:{
      'Content-type':'application/json'
    },
    body:JSON.stringify(one)
  }).then(res=>console.log(res)).catch(res=>console.log(res))

 }